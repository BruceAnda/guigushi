package cn.zhaoliang5156.guigushikotlindev.ext

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(layoutId: Int, attchToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attchToRoot)
}
