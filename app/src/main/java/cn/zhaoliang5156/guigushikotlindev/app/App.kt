package cn.zhaoliang5156.guigushikotlindev.app

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco

/**
 * Application
 */
class App : Application() {

    override fun onCreate() {
        super.onCreate()

        init()
    }

    /**
     * 初始化
     */
    private fun init() {
        initFresco()
    }

    /**
     * 初始化Fresco
     */
    private fun initFresco() {
        Fresco.initialize(this)
    }
}