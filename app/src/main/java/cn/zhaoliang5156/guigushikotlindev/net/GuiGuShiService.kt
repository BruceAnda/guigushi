package cn.zhaoliang5156.guigushikotlindev.net

import cn.zhaoliang5156.guigushikotlindev.main.bean.MainResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * 鬼故事 接口服务
 */
interface GuiGuShiService {

    @GET("955-1?showapi_appid=69652&showapi_sign=a8c16eb1dbb842d987087416535faf79")
    fun getGuGuShi(@Query("type") type: String, @Query("page") page: Int): Observable<MainResponse>
}