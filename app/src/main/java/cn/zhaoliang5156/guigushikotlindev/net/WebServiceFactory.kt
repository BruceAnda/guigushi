package cn.zhaoliang5156.guigushikotlindev.net

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private val bastUrl = "http://route.showapi.com/"

/**
 * Api 工厂
 */
object WebServiceFactory {

    fun getLogInterceptor(): Interceptor {
        val logerInterceptor = HttpLoggingInterceptor()
        logerInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return logerInterceptor
    }

    fun <T> createRetorfitService(clazz: Class<T>): T {

        val client = OkHttpClient
                .Builder()
                .addInterceptor(getLogInterceptor())
                .build()

        val retorfit = Retrofit
                .Builder()
                .baseUrl(bastUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return retorfit.create(clazz)
    }
}