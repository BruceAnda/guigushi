package cn.zhaoliang5156.guigushikotlindev.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import cn.zhaoliang5156.guigushikotlindev.R
import cn.zhaoliang5156.guigushikotlindev.main.bean.MainItem
import cn.zhaoliang5156.guigushikotlindev.net.GuiGuShiService
import cn.zhaoliang5156.guigushikotlindev.net.WebServiceFactory
import com.bawei.alguigushikotlin.main.adapter.MainAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import me.yuqirong.cardswipelayout.CardItemTouchHelperCallback
import me.yuqirong.cardswipelayout.CardLayoutManager

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.simpleName
    private var items: ArrayList<MainItem> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var adapter = MainAdapter(items)
        recycler_view.adapter = adapter

        // recycler_view.layoutManager = LinearLayoutManager(this)
        // recycle_view.layoutManager = CardLayoutManager(recycle_view, )
        var callback = CardItemTouchHelperCallback(recycler_view.adapter, items)
        var touchHelper = ItemTouchHelper(callback)
        recycler_view.layoutManager = CardLayoutManager(recycler_view, touchHelper)
        touchHelper.attachToRecyclerView(recycler_view)

        WebServiceFactory.createRetorfitService(GuiGuShiService::class.java).getGuGuShi("dp", 1)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe {
                    Log.i(TAG, "----" + it.showapi_res_body.pagebean.contentlist.size)
                    items.addAll(it.showapi_res_body.pagebean.contentlist)
                    adapter.notifyDataSetChanged()
                }
    }
}
