package com.bawei.alguigushikotlin.main.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import cn.zhaoliang5156.guigushikotlindev.R
import cn.zhaoliang5156.guigushikotlindev.ext.inflate
import cn.zhaoliang5156.guigushikotlindev.main.bean.MainItem
import kotlinx.android.synthetic.main.main_item.view.*

class MainAdapter(var items: ArrayList<MainItem>) : RecyclerView.Adapter<MainAdapter.MainHolder>() {
    override fun onBindViewHolder(holder: MainHolder, position: Int) {
        holder?.bind(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainHolder = MainHolder(parent?.inflate(R.layout.main_item))

    override fun getItemCount(): Int = items.size

    class MainHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        fun bind(item: MainItem) {
            // Glide.with(itemView.context).load(item.img).into(itemView.iv_image)
            //itemView.iv_image.load(item.img)
            itemView.fresco_image.setImageURI(item.img)
            itemView.tv_title.text = item.title
            itemView.tv_desc.text = item.desc
        }
    }
}