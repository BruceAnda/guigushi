package cn.zhaoliang5156.guigushikotlindev.main.bean

class MainResponse(
        var showapi_res_code: Int,
        var showapi_res_error: String,
        var showapi_res_body: ResponseBody
)

class ResponseBody(
        var ret_code: Int,
        var pagebean: PageBean,
        var maxResult: String
)

class PageBean(
        var allPages: String,
        var contentlist: ArrayList<MainItem>
)

class MainItem(
        var title: String,
        var desc: String,
        var link: String,
        var img: String
)



